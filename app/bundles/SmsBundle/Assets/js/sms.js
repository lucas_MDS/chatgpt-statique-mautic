/** SmsBundle **/
Mautic.smsOnLoad = function (container, response) {
    const smsMessage = mQuery('#sms_message');

    if (smsMessage.length) {
        Mautic.setSmsCharactersCount(smsMessage);
        smsMessage.on('input', () => {
            Mautic.setSmsCharactersCount(smsMessage);
        });
    }

    if (mQuery(container + ' #list-search').length) {
        Mautic.activateSearchAutocomplete('list-search', 'sms');
    }

    if (mQuery('table.sms-list').length) {
        var ids = [];
        mQuery('td.col-stats').each(function () {
            var id = mQuery(this).attr('data-stats');
            ids.push(id);
        });

        // Get all stats numbers in batches of 10
        while (ids.length > 0) {
            let batchIds = ids.splice(0, 10);
            Mautic.ajaxActionRequest(
                'sms:getSmsCountStats',
                {ids: batchIds},
                function (response) {
                    if (response.success && response.stats) {
                        for (var i = 0; i < response.stats.length; i++) {
                            var stat = response.stats[i];
                            if (mQuery('#pending-' + stat.id).length) {
                                if (stat.pending) {
                                    mQuery('#pending-' + stat.id + ' > a').html(stat.pending);
                                    mQuery('#pending-' + stat.id).removeClass('hide');
                                }
                            }
                        }
                    }
                },
                false,
                true
            );
        }
    }
};

Mautic.setSmsCharactersCount = function (smsMessage) {
    mQuery('#sms_nb_char').text((smsMessage.val().length))
};

Mautic.selectSmsType = function(smsType) {
    if (smsType == 'list') {
        mQuery('#leadList').removeClass('hide');
        mQuery('#publishStatus').addClass('hide');
        mQuery('.page-header h3').text(mauticLang.newListSms);
    } else {
        mQuery('#publishStatus').removeClass('hide');
        mQuery('#leadList').addClass('hide');
        mQuery('.page-header h3').text(mauticLang.newTemplateSms);
    }

    mQuery('#sms_smsType').val(smsType);

    mQuery('body').removeClass('noscroll');

    mQuery('.sms-type-modal').remove();
    mQuery('.sms-type-modal-backdrop').remove();
};

Mautic.standardSmsUrl = function(options) {
    if (!options) {
        return;
    }

    var url = options.windowUrl;
    if (url) {
        var editEmailKey = '/sms/edit/smsId';
        if (url.indexOf(editEmailKey) > -1) {
            options.windowUrl = url.replace('smsId', mQuery('#campaignevent_properties_sms').val());
        }
    }

    return options;
};

Mautic.disabledSmsAction = function(opener) {
    if (typeof opener == 'undefined') {
        opener = window;
    }

    var sms = opener.mQuery('#campaignevent_properties_sms').val();

    var disabled = sms === '' || sms === null;

    opener.mQuery('#campaignevent_properties_editSmsButton').prop('disabled', disabled);
};


Mautic.generateTextWithChatGPT = function(model,smsCount) {
    Mautic.ajaxActionRequest(
        'sms:generateTextWithChatGPT',
        {},
        function (response) {

            const userMessage = mQuery('#sms_message').val();
            const openai_endpoint = "https://api.openai.com/v1/chat/completions";
            const openai_token = "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";
            const regex = '/\bhttps?:\/\/\S+\b/';
            let maxToken = 2048;

            let baseTextarea = null
            if(model == "gpt-3.5-turbo"){
                baseTextarea = '#smsform_plainTextGPT3'
            }
            else if (model == "gpt-4"){
                baseTextarea = '#smsform_plainTextGPT4'
            }

            if(smsCount > 0){ //Si 0, pas de limite de caractère pour la varation, on garde maxToken
                maxToken = 65 * smsCount //65 tokens correspond à environ 160 caractères, mais jamais très précis
            }

            let matches = userMessage.matchAll(regex);
            let links = [];

            for (const match of matches) {
                links.push(match[0]);
            }

            let systemPrompt = "Je veux que tu n'écrives que des réponses en Français. Ton objectif est de créer une variation du " +
                "texte qui t'es présenté, en gardant une certaine liberté sur le vocabulaire utilisé et sur les tournures de phrases. " +
                "N'hésite pas à les modifier de façon professionnelle. Ta réponse doit être concise, informative, facile à lire et à comprendre. " +
                "La variation que tu proposes doit être raccourcie si nécessaire, en utilisant environ " + maxToken + " tokens. " +
                "Il Si il existe des liens http, conserve les tels qu'ils sont, ils NE doivent PAS avoir de MODIFICATION! " +
                "Si les liens se succèdent, ne pas rajouter de connecteur (et). " +
                // "Par exemple, pour les liens à ne pas faire : " +
                // "'Bonjour! N'oubliez pas de profiter de notre offre spéciale du jour: https://127.0.0.1:8000/ et visitez elcalidrow.com/e=ieoa84ue84.', " +
                "ne pas générer de mots entre les liens. Un bon exemple : 'offre spéciale du jour: https://127.0.0.1:8000/ https://elcalidrow.com/e=ieoa84ue84'," +
                "il ne doit rien y avoir entre les différents liens s'il n'y a rien. Si le message se termine par un lien, ne pas finir avec un point. " +
                "Arrête de mettre des points. Si il y a des numéros de téléphone, des suites de chiffres, des numéros, conserve les tels qu'ils sont, sans modifications. " +
                "Si il y a un 'STOP SMS', Ne modifie rien.";



            for (let i = 0; i < 3; i++) {
                let textarea = baseTextarea + i
                const requestData = {
                    messages: [
                        {
                            role: "system",
                            content: systemPrompt,
                        },
                        {
                            role: "user",
                            content: userMessage
                        }
                    ],
                    model: model,

                    max_tokens: 50,
                    temperature: 0.9
                }
                if (!mQuery(textarea).hasClass('init')) {
                    mQuery(textarea)
                        .addClass('init')
                        .removeClass('hide');

                    mQuery('.plaintext-spinner').removeClass('hide');

                    const handleComplete = () => {
                        mQuery(textarea)
                            .removeClass('init');

                        mQuery('.plaintext-spinner')
                            .addClass('hide');

                        if (mQuery(textarea).hasClass('firstQuery')) {
                            mQuery(textarea).addClass('hide');
                        }
                    };

                    fetch(openai_endpoint, {
                        method: "POST",
                        headers: {
                            "Content-Type": "application/json",
                            Authorization: "Bearer " + openai_token,
                        },
                        body: JSON.stringify(requestData),
                    })
                        .then((response) => {
                            console.log(response);
                            if (!response.ok) {
                                throw new Error(`HTTP error! Status: ${response.status}, Message: ${response.statusText}`);
                            }
                            return response.json();
                        })
                        .then((data) => {
                            if (data.error) {
                                console.error("Error: " + data.error.message);
                                if (mQuery(textarea).hasClass('firstQuery')) {
                                    mQuery(textarea).addClass('hide');
                                }
                            } else {
                                const apiResponse = data.choices[0].message.content;
                                let hasLinks = true



                                mQuery(textarea)
                                    .text(apiResponse)
                                    .removeClass('firstQuery');
                            }
                            handleComplete();
                        })
                        .catch((error) => {
                            console.error("Fetch error: ", error);
                            handleComplete();
                        });
                }
            }
        }
    )
}